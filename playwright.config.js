// playwright.config.js
// @ts-check

// import { PlaywrightTestConfig } from '@playwright/test';
// const config: = {
//     use: {
//         headless: false,
//         ignoreHTTPSErrors: true,
//         //baseURL: 'https://10.0.2.30',
    
//       },
   
//     };
//   module.exports = config;


const config = {
  use: {
    headless: false,
    viewport: { width: 1280, height: 720 },
    ignoreHTTPSErrors: true,
    video: 'on-first-retry',
    baseURL: 'https://10.0.2.30',
  },
};

module.exports = config;