Feature: login with admin user

Scenario Outline: login with Admin user

Given Admin user login to QE tool "<URL>"

Examples:
    | URL| Header 2 | Header 3 |
    | https://10.0.2.30/datalake/#/ | Value 2  | Value 3  |


Scenario Outline: login with non Admin user

Given non Admin user login to QE tool "<URL>"

Examples:
    | URL| Header 2 | Header 3 |
    | https://10.0.2.30/datalake/#/| Value 2  | Value 3  |