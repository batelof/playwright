const { test, expect } = require('@playwright/test');

const {
    Given,
    When,
    Then,
    BeforeAll,
    AfterAll,
    setDefaultTimeout,
  } = require("cucumber");
  const { chromium } = require("playwright");
  const playwright= require('playwright');
  const { assert } = require("chai");
 

let page;
let browser;

setDefaultTimeout(50 * 1000);

// BeforeAll(async () => {
//   browser = process.env.GITHUB_ACTIONS
//     ? await chromium.launch({ headless: true })
//     : await chromium.launch({ headless: false });
//   page = await browser.newPage();
// });



  Given('Admin user login to QE tool {string}', async (URL) =>{
    // Write code here that turns the phrase above into concrete actions

const browser = await playwright.chromium.launch()

const context = await browser.newContext ({

    //ignoreHTTPSErrors: true,

    httpCredentials :{
       
      username :"mdclone.admin",
      password: "Mmm@24680"
    }
  })
    
  
  const page = await context.newPage();
    
  await page.goto(URL);

  //wait for page to load
  await page.waitForLoadState('domcontentloaded');
  
  //find system setting if exist for Admin user
  const locator = page.locator('text=System Settings');
  await expect(locator).toBeVisible();

  // go to System Settings page
  await locator.click()

  await page.waitForSelector ('text=System Settings');

  //verify title is correct
  const title =  await page.innerText('h1')
  expect(title).toBe("SYSTEM SETTINGS")
  await page.waitForTimeout(3000);

  await browser.close()

  });


  Given('non Admin user login to QE tool {string}', async (URL) =>{
    // Write code here that turns the phrase above into concrete actions
    const browser = await playwright.chromium.launch()

    const context = await browser.newContext ({
    
        //ignoreHTTPSErrors: true,
    
        httpCredentials :{
           
          username :"batel.ofir",
          password: "Mmm@24680"
        }
      })
        
      const page = await context.newPage();
        
      await page.goto(URL);
    
      //wait for page to load
      await page.waitForLoadState('domcontentloaded');
      
      //find system setting if exist for Admin user

    const result = await page.isVisible('text=System Settings')
    if (!result) console.log("Could not find system setting in the menu")
    
    await page.waitForTimeout(3000);

    await browser.close()

  });

